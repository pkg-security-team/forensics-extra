#!/bin/bash

# find-x11.sh -- find packages FED|FER|FES depending of x11-common
#                (These packages should be referenced as FGD|FGR|FGS)
#
# This file is part of the forensics-extra and is useful for debugs.
# This file is based in find-deps.sh.
#
# Copyright 2024 Joao Eriberto Mota Filho <eriberto@debian.org>
#
# You can use this program under the BSD-3-Clause conditions.

####################
### Main program ###
####################

### Is there list-of-packages-extra?

[ -e list-of-packages-extra ] || { echo -e "\nI can't find list-of-packages-extra. Aborting.\n"; exit 1; }

### Go!

LISTFILES=$(cat list-of-packages-extra | egrep -v "^#" | egrep '(FED|FER|FES)' | egrep -v scrot | cut -d" " -f1 | tr '|' ' ')

# Pre-checking

apt-get install --no-install-recommends -s $LISTFILES | grep '^Inst' | grep x11-common > /dev/null || \
    { echo -e "\nNot found packages depending of x11-common.\nNote this script only checks packages set as FED, FER and FES.\n"; exit 0; }

# Package by package check

for i in $(echo $LISTFILES)
do
    echo $i; apt-get install --no-install-recommends -s $i | grep '^Inst' | grep x11-common && \
    { echo -e "\nBreaking...\n\nI found a package FED|FER|FES depending of x11-common: $i.\nPlease, fix list-of-files-extra and run find-x11.sh again."; break; }
done

exit 0
