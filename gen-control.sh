#!/bin/bash

# gen-control.sh -- generates debian/control and debian/README.Debian files
#
# This file is part of the forensics-extra and is based on file with the same
# name from forensics-all package.
#
# Copyright 2018-2019 Joao Eriberto Mota Filho <eriberto@debian.org>
#
# You can use this program under the BSD-3-Clause conditions.

#################
#################
### Variables ###
#################
#################

[ -e variables ] || { echo "I can't find variables file. Aborting."; exit 1; }

source variables

####################
####################
### Main program ###
####################
####################

### Initial check

[ -e /usr/bin/wrap-and-sort ] || { echo "ERROR: wrap-and-sort command not found"; exit 1; }

[ -e list-of-packages-extra ] || { echo "I can't find list-of-packages-extra file. Aborting."; exit 1; }

function synopsis-check() {
    [ -e synopsis.ERROR ] && { echo -e "\n\nERROR: I found synopsis.ERROR file. Aborting.\n"; exit 1; }
}

synopsis-check

[ -d build ] || mkdir build


### Initial message

echo -e "\ngen-control.sh -- generates debian/control file"
echo " "
echo "The 'detect-conflicts.sh' should be used before this script. "
echo " "
echo -e "You can edit the \e[5mvariables\e[25m file to set some data."
echo " "
echo "Press ENTER to continue or Ctrl-C to abort."
read NOTHING

#################
# forensics-all #
#################

### Showing...

# Depends

echo '  __                          _                          _             '
echo ' / _| ___  _ __ ___ _ __  ___(_) ___ ___        _____  _| |_ _ __ __ _ '
echo '| |_ / _ \|  __/ _ \  _ \/ __| |/ __/ __|_____ / _ \ \/ / __|  __/ _` |'
echo '|  _| (_) | | |  __/ | | \__ \ | (__\__ \_____|  __/>  <| |_| | | (_| |'
echo '|_|  \___/|_|  \___|_| |_|___/_|\___|___/      \___/_/\_\\__|_|  \__,_|'


echo -e "\n\n-------------------------------------------------------------------------"
echo "The packages to be put in DEPENDS field (debian/control) are listed below"
echo "-------------------------------------------------------------------------"
echo " "
cat list-of-packages-extra | egrep FED | egrep '^[a-z]' | cut -d" " -f1 > build/depends
cat build/depends | xargs
echo " "
echo "TOTAL: $(cat build/depends | wc -l) packages."
echo "Press ENTER to continue..."
read NOTHING

# Suggests

cat list-of-packages-extra | egrep '( FS|FES)' | egrep -v "^forensics-extra FS$" | egrep '^[a-z]' | \
    cut -d" " -f1 > build/suggests

echo -e "\n\n----------------------------------------------------"
echo "For SUGGESTS field, there are the following packages"
echo -e "----------------------------------------------------\n"

if [ -s build/suggests ]
then
    cat build/suggests | xargs
else
    echo "No packages for this field."
fi

# Recommends

cat list-of-packages-extra | egrep FER | egrep '^[a-z]' | cut -d" " -f1 > build/recommends

echo -e "\n\n------------------------------------------------------"
echo "For RECOMMENDS field, there are the following packages"
echo -e "------------------------------------------------------\n"

if [ -s build/recommends ]
then
    cat build/recommends | xargs
else
    echo "No packages for this field."
fi

# Ignored

cat list-of-packages-extra | egrep FI'( |$)' | egrep '^[a-z]' | cut -d" " -f1 > build/ignored

echo -e "\n\n--------------------------------------"
echo "The following packages will be IGNORED"
echo -e "--------------------------------------\n"

if [ -s build/ignored ]
then
    cat build/ignored | xargs
else
    echo -e "No packages to be ignored."
fi

# Waitting

echo -e "\n\nPress ENTER to continue or Ctrl-C to abort."
echo " "
read NOTHING

### Build the partial debian/control file

cat templates/control.part1 | sed "s/SV-VALUE/$STD_VER/" | \
    sed "s/DH-VALUE/$DH_LEVEL/" > control.NEW

# Suggests
if [ -s build/suggests ]
then
    echo "Suggests: $(cat build/suggests | xargs | sed 's/ /, /g' | \
    sed 's/ /\n/g')" | sed 's/^/          /' | \
    sed 's/ \+Suggests:/Suggests:/' >> control.NEW
fi

# Recommends
if [ -s build/recommends ]
then
    echo "Recommends: $(cat build/recommends | xargs | sed 's/ /, /g' | \
    sed 's/ /\n/g')" | sed 's/^/          /' | \
    sed 's/ \+Recommends:/Recommends:/' >> control.NEW
fi

# Depends
echo "Depends: $(cat build/depends | xargs | sed 's/ /, /g' | \
   sed 's/$/,/' | sed 's/ /\n/g')" | sed 's/^/         /' | \
   sed 's/ \+Depends:/Depends:/' >> control.NEW

# Final part
cat templates/control.part2 >> control.NEW

cat build/depends | xargs | sed 's/ /, /g' | sed 's/,$/./' | fold -sw 69 | \
    sed 's/^/   /' | sed 's/|/ or /g' | sed 's/ $//' >> control.NEW

cat templates/control.part3 >> control.NEW



echo '  __                           _                               _ '
echo ' / _| ___  _ __       _____  _| |_ _ __ __ _        __ _ _   _(_)'
echo '| |_ / _ \|  __|____ / _ \ \/ / __|  __/ _  |_____ / _  | | | | |'
echo '|  _| (_) | | |_____|  __/>  <| |_| | | (_| |_____| (_| | |_| | |'
echo '|_|  \___/|_|(_)     \___/_/\_\\__|_|  \__,_|      \__, |\__,_|_|'
echo '                                                   |___/         '

echo -e "\n\n-------------------------------------------------------------------------"
echo "The packages to be put in DEPENDS field (debian/control) are listed below"
echo "-------------------------------------------------------------------------"
echo " "
cat list-of-packages-extra | egrep 'FGD' | egrep '^[a-z]' | cut -d" " -f1 > build/depends2
cat build/depends2 | xargs
echo " "
echo "TOTAL: $(cat build/depends2 | wc -l) packages."
echo "Press ENTER to continue..."
read NOTHING

# Header of package
cat templates/control.part4 >> control.NEW

# Suggests
cat list-of-packages-extra | egrep '( FS|FGS)' | egrep -v "^forensics-extra-gui FS$" | \
    egrep '^[a-z]' | cut -d" " -f1 > build/suggests2

echo -e "\n\n----------------------------------------------------"
echo "For SUGGESTS field, there are the following packages"
echo -e "----------------------------------------------------\n"

if [ -s build/suggests2 ]
then
    cat build/suggests2 | xargs
else
    echo "No packages for this field."
fi

# Recommends

cat list-of-packages-extra | egrep FGR | egrep '^[a-z]' | cut -d" " -f1 > build/recommends2

echo -e "\n\n------------------------------------------------------"
echo "For RECOMMENDS field, there are the following packages"
echo -e "------------------------------------------------------\n"

if [ -s build/recommends2 ]
then
    cat build/recommends2 | xargs
else
    echo "No packages for this field."
fi

# Waitting

echo -e "\n\nPress ENTER to continue or Ctrl-C to abort."
echo " "
read NOTHING

### Building

# Suggests
if [ -s build/suggests2 ]
then
    echo "Suggests: $(cat build/suggests2 | xargs | sed 's/ /, /g' | \
    sed 's/ /\n/g')" | sed 's/^/          /' | \
    sed 's/ \+Suggests:/Suggests:/' >> control.NEW
fi

# Recommends
if [ -s build/recommends2 ]
then
    echo "Recommends: $(cat build/recommends2 | xargs | sed 's/ /, /g' | \
    sed 's/ /\n/g')" | sed 's/^/          /' | \
    sed 's/ \+Recommends:/Recommends:/' >> control.NEW
fi

# Depends
echo "Depends: $(cat build/depends2 | xargs | sed 's/ /, /g' | \
   sed 's/$/,/' | sed 's/ /\n/g')" | sed 's/^/         /' | \
   sed 's/ \+Depends:/Depends:/' >> control.NEW

# Final part
cat templates/control.part5 >> control.NEW

cat build/depends2 | xargs | sed 's/ /, /g' | sed 's/,$/./' | fold -sw 69 | \
    sed 's/^/   /' | sed 's/|/ or /g' | sed 's/ $//' >> control.NEW

cat templates/control.part3 >> control.NEW


echo '  __                          _                 __       _ _ '
echo ' / _| ___  _ __ ___ _ __  ___(_) ___ ___       / _|_   _| | |'
echo '| |_ / _ \|  __/ _ \  _ \/ __| |/ __/ __|_____| |_| | | | | |'
echo '|  _| (_) | | |  __/ | | \__ \ | (__\__ \_____|  _| |_| | | |'
echo '|_|  \___/|_|  \___|_| |_|___/_|\___|___/     |_|  \__,_|_|_|'

cat templates/control.part6 >> control.NEW

echo -e '\n\nCreated forensics-full section.\n'

wrap-and-sort -f ./control.NEW

echo -e "\n\n---> Created control.NEW..."

################################
################################
### Create README.Debian.NEW ###
################################
################################

# forensics-all.README.Debian
echo -e "\n\n---> Creating forensics-extra.README.Debian.NEW..."
echo "forensics-extra for Debian" > forensics-extra.README.Debian.NEW
echo "--------------------------" >> forensics-extra.README.Debian.NEW
echo -e "\nLIST OF PACKAGES INSTALLED BY forensics-extra" >> forensics-extra.README.Debian.NEW
echo -e "\n\nCOMMON PACKAGES (available for all machines)\n"  >> forensics-extra.README.Debian.NEW
./synopsis.sh FED >> forensics-extra.README.Debian.NEW
echo -e "\nRECOMMENDED PACKAGES (available for some architectures only)\n" >> forensics-extra.README.Debian.NEW

TEST=$(cat list-of-packages-extra | grep FER)
if [ "$TEST" ]
then
    ./synopsis.sh FER >> forensics-extra.README.Debian.NEW
else
    echo "No packages for this field at this moment." >> forensics-extra.README.Debian.NEW
fi

synopsis-check

echo -e "\n" >> forensics-extra.README.Debian.NEW
echo " -- $DEBFULLNAME <$DEBEMAIL>  $(LANG=C date -R)" >> forensics-extra.README.Debian.NEW

# forensics-extra-gui.README.Debian
echo -e "\n\n---> Creating forensics-extra-gui.README.Debian.NEW..."
echo "forensics-extra-gui for Debian" > forensics-extra-gui.README.Debian.NEW
echo "------------------------------" >> forensics-extra-gui.README.Debian.NEW
echo -e "\nLIST OF PACKAGES INSTALLED BY forensics-extra-gui" >> forensics-extra-gui.README.Debian.NEW
echo -e "\n\nCOMMON PACKAGES (available for all machines)\n"  >> forensics-extra-gui.README.Debian.NEW
./synopsis.sh FGD >> forensics-extra-gui.README.Debian.NEW
echo -e "\nRECOMMENDED PACKAGES (available for some architectures only)\n" >> forensics-extra-gui.README.Debian.NEW

TEST=$(cat list-of-packages-extra | grep FGR)
if [ "$TEST" ]
then
    ./synopsis.sh FGR >> forensics-extra-gui.README.Debian.NEW
else
    echo "No packages for this field at this moment." >> forensics-extra-gui.README.Debian.NEW
fi

synopsis-check

echo -e "\n" >> forensics-extra-gui.README.Debian.NEW
echo " -- $DEBFULLNAME <$DEBEMAIL>  $(LANG=C date -R)" >> forensics-extra-gui.README.Debian.NEW

### Final message

echo -e "\n\n\n"
echo "The control.NEW, forensics-extra.README.Debian.NEW and"
echo "forensics-extra-gui.README.Debian.NEW files are done."
echo "Please, remove the NEW suffixes and move to debian/"
echo "directory. You can use the move.sh script for this."
echo " "

### Check for transitional

TEST=$(egrep -i transitional *.README.Debian.NEW)

if [ "$TEST" ]
then
    echo -e "\n\n\n"
    echo -e "\e[5mWARNING:\e[25m there is/are transitional package(s):"
    egrep -i transitional *.README.Debian.NEW | cut -d: -f2
fi

### Remove the build directory

[ -d build ] && rm -rf build
