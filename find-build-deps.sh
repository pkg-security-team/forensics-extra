#!/bin/bash

# find-build-deps.sh -- find packages build depending of a specific package
#
# This file is part of the forensics-extra and is useful for debugs.
# This file is based in find-build-deps.sh from forensics-all package.
#
# Copyright 2022-2024 Joao Eriberto Mota Filho <eriberto@debian.org>
#
# You can use this program under the BSD-3-Clause conditions.

####################
### Main program ###
####################

### Is there $1?

[ "$1" ] || { \
    echo -e "\nfind-build-deps.sh -- find packages build depending of a specific package\n"; \
    echo -e "You must provide a build dependency name. I will search for packages using this\nbuild dependency. My source is the file list-of-packages-extra and I will use grep.\n\nAborting.\n"; \
    exit 1; }
[ -e list-of-packages-extra ] || { echo -e "\nI can't find list-of-packages-extra. Aborting.\n"; exit 1; }

### Go!

LISTFILES=$(cat list-of-packages-extra | egrep -v "^#" | egrep '(FED|FGD)' | cut -d" " -f1 | tr '|' ' ')

for i in $(echo $LISTFILES)
do
    echo $i; apt-get build-dep -s $i | grep '^Inst' | grep $1 && \
    { echo -e "\nBreaking...\n\nI found a package build depending of $1: $i.\nPlease, fix list-of-files-extra and run find-build-deps.sh again."; break; }
done

exit 0
