#!/bin/bash

# synopsis.sh -- show short description for packages inside list-of-packages
#
# This file is part of the forensics-extra and is based on file with the same
# name from forensics-all package.
#
# Copyright 2018-2019 Joao Eriberto Mota Filho <eriberto@debian.org>
#
# You can use this program under the BSD-3-Clause conditions.

####################
### Main program ###
####################

### Initial check. We are in right place?

[ -e list-of-packages-extra ] || { echo "I can't find list-of-packages-extra file. Aborting."; exit 1; }

### Go!

# Help
function help() {
    echo -e "\nUsage: synopsis.sh [OPTION]"
    echo -e "\n-h  Show help."
    echo "FED  Show short description for packages in Depends field (forensics-extra)."
    echo "FER  Show short description for packages in Recommends field (forensics-extra)."
    echo "FGD  Show short description for packages in Depends field (forensics-extra-gui)."
    echo "FGR  Show short description for packages in Recommends field (forensics-extra-gui)."
    echo "<empty>  Show short description for all packages in list-of-packages."
    echo " "
    exit 1
}

[ "$1" != "FED" -a "$1" != "FER" -a "$1" != "FGD" -a "$1" != "FGR" -a ! -z "$1" ] && help

# Operation mode
[ "$1" = "FED" -o "$1" = "FER" -o "$1" = "FGD" -o "$1" = "FGR" ] && INFO="$1" || INFO=" "

# List of packages in local list-of-packages file.
FILES=$(cat list-of-packages-extra | egrep "$INFO" | cut -d" " -f1  | egrep '^[a-z0-9]')

# The synopsis...
function error() {
    echo -e "\nERROR: package $i not found. Aborting."
    echo -e "\nERROR: package $i not found. Aborting." > synopsis.ERROR
    exit 1
}

for i in $FILES
do
    LANG=C apt-cache search --names-only $i | egrep "^$i -" || error
done

exit 0
