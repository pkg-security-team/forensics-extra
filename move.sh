#!/bin/bash

# move.sh -- move control.NEW and *.README.Debian.NEW to debian/ directory
#
# This file is part of the forensics-extra and is based on file with the same
# name from forensics-all package.
#
# Copyright 2018-2019 Joao Eriberto Mota Filho <eriberto@debian.org>
#
# You can use this program under the BSD-3-Clause conditions.

####################
### Main program ###
####################

### Initial check. We are in right place?

[ -e control.NEW ] || { echo "I can't find control.NEW. Aborting."; exit 1; }
[ -e forensics-extra.README.Debian.NEW -o "$1" = "-f" ] || \
     { echo -e "\nI can't find forensics-extra.README.Debian.NEW. Aborting. Use -f to ignore."; exit 1; }
[ -e forensics-extra-gui.README.Debian.NEW -o "$1" = "-f" ] || \
     { echo -e "\nI can't find forensics-extra-gui.README.Debian.NEW. Aborting. Use -f to ignore."; exit 1; }

### Go!

mv control.NEW debian/control
[ -e forensics-extra.README.Debian.NEW ] && mv forensics-extra.README.Debian.NEW debian/forensics-extra.README.Debian
[ -e forensics-extra-gui.README.Debian.NEW ] && mv forensics-extra-gui.README.Debian.NEW debian/forensics-extra-gui.README.Debian

echo -e "\nFiles control.NEW and *.README.Debian.NEW were moved to \ndebian/ place and renamed to control and *.README.Debian.\n"

exit 0
