#!/bin/bash

# detect-conflicts.sh -- show packages provided by forensics-all* and
# forensics-extra*
#
# This file is part of the forensics-extra.
#
# Copyright 2019 Joao Eriberto Mota Filho <eriberto@debian.org>
#
# You can use this program under the BSD-3-Clause conditions.

####################
### Main program ###
####################

### Check for the right path
[ -e list-of-packages-extra ] || { echo -e "\nI can't find list-of-packages-extra. Aborting.\n"; exit 1; }

### Is there -f option?
[ -d tmp-detect -a "$1" = "-f" ] && rm -rf tmp-detect

### Is there a tmp-detect/ directory?
[ -d tmp-detect ] \
    && { echo "I found tmp-detect/ directory. Please, remove it or use -f option."; exit 1; } \
    || mkdir tmp-detect

### Download the last 'list-of-packages' from forensics-all package. Source: salsa.
cd tmp-detect
wget https://salsa.debian.org/pkg-security-team/forensics-all/raw/debian/master/list-of-packages || exit 1
cd ..

### Compare list-of-packages (from forensics-all) and list-of-packages-extra.
### The folloing packages must exist in both lists: forensics-all, forensics-all-gui,
### forensics-extra and forensics-extra-gui
cat list-of-packages-extra | egrep ^[a-z0-9] | cut -d" " -f1 > tmp-detect/list
cat tmp-detect/list-of-packages | egrep ^[a-z0-9] | cut -d" " -f1 > tmp-detect/list2
cat tmp-detect/list tmp-detect/list2 | sort | uniq -d | \
    egrep -v '(forensics-all.*|forensics-extra.*|forensics-full)' > tmp-detect/list3

### Show results
echo -e "\n\n\n------------------"
echo -e "The following packages are present in forensics-all and forensics-extra sources:\n"

if [ -s tmp-detect/list3 ]
then
    cat tmp-detect/list3
else
    echo "No conflicts found."
fi

echo " "

# FIXME
TEST=$(cat list-of-packages-extra | grep FIXME | egrep "^[a-z0-9]")
echo -e "\nThe following packages are marked as \e[5mFIXME\e[25m:\n"
if [ "$TEST" ]
then
    cat list-of-packages-extra | grep FIXME | egrep "^[a-z0-9]" | cut -d" " -f1
else
    echo "No packages at this moment."
fi
echo " "

# Remove tmp directory
[ -d tmp-detect ] && rm -rf tmp-detect

exit 0
